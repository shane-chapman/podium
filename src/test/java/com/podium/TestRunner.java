package com.podium;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "src/test/java/com/podium/api/features", "src/test/java/com/podium/gui/features"}, glue = { "com.podium.api.steps", "com.podium.gui.steps" }, plugin = {
		"pretty", "json:target/cucumber-report/cucumber.json", "html:target/cucumber-report" }, monochrome = true)

public class TestRunner extends AbstractTestNGCucumberTests {
	
	public static WebDriver driver;
	
	@AfterSuite
	public void afterSuite() {

	}
	
}
