Feature: API - Products and Applications API responses
  Description: Scenarios to assert that the correct responses are received from the Products and Applications APIs
  
  Scenario: Assert that an error is received if the product id is not valid in the applications request
    Given An API call is made for product "0"
    Then Product not found by that ID error is recieved

  Scenario: Assert that an accepted decision is received for product 1 when all product rules are met
    Given An API call is made for product "1"
    And All product rules are met
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 1 when the customers age is equal to the minimum age rule
    Given An API call is made for product "1"
    And The customers age is equal to the minimum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 1 when the customers age is equal to the maximum age rule
    Given An API call is made for product "1"
    And The customers age is equal to the maximum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a rejected decision is received for product 1 when the minimum age rule is not met
    Given An API call is made for product "1"
    And The minAge rule is not met
    Then "rejected" decision is recieved

  Scenario: Assert that a rejected decision is received for product 1 when the maximum age rule is not met
    Given An API call is made for product "1"
    And The maxAge rule is not met
    Then "rejected" decision is recieved

  Scenario: Assert that an accepted decision is received for product 2 when all product rules are met
    Given An API call is made for product "2"
    And All product rules are met
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 2 when the customers age is equal to the minimum age rule
    Given An API call is made for product "2"
    And The customers age is equal to the minimum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 2 when the customers age is equal to the maximum age rule
    Given An API call is made for product "2"
    And The customers age is equal to the maximum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a rejected decision is received for product 2 when the minimum age rule is not met
    Given An API call is made for product "2"
    And The minAge rule is not met
    Then "rejected" decision is recieved

  Scenario: Assert that a rejected decision is received for product 2 when the maximum age rule is not met
    Given An API call is made for product "2"
    And The maxAge rule is not met
    Then "rejected" decision is recieved

  Scenario: Assert that an accepted decision is received for product 3 when all product rules are met
    Given An API call is made for product "3"
    And All product rules are met
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 3 when the customers age is equal to the minimum age rule
    Given An API call is made for product "3"
    And The customers age is equal to the minimum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a accepted decision is received for product 3 when the customers age is equal to the maximum age rule
    Given An API call is made for product "3"
    And The customers age is equal to the maximum age rule
    Then "accepted" decision is recieved

  Scenario: Assert that a rejected decision is received for product 3 when the minimum age rule is not met
    Given An API call is made for product "3"
    And The minAge rule is not met
    Then "rejected" decision is recieved

  Scenario: Assert that a rejected decision is received for product 3 when the maximum age rule is not met
    Given An API call is made for product "3"
    And The maxAge rule is not met
    Then "rejected" decision is recieved
