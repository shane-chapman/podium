package com.podium.api.steps;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import com.podium.configuration.config;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import junit.framework.Assert;

public class api_steps extends config {

	static HashMap<String, String> product1;
	static HashMap<String, String> product2;
	static HashMap<String, String> product3;
	static int product_id;
	static int age, amount, value;
	static Float actual_ltv;
	static Float expected_ltv;

	public static Response doGetRequest(String endpoint) {
		RestAssured.defaultParser = Parser.JSON;

		return given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).when().get(endpoint).then()
				.contentType(ContentType.JSON).extract().response();
	}

	@Before
	public void getProducts() {
		RestAssured.baseURI = "https://podium-slns-interview.getsandbox.com";
		RestAssured.defaultParser = Parser.JSON;
		Response response = doGetRequest("/products");

		String data = response.jsonPath().getString("data").replace(":[", "").replace("]", "").replace("[[", "")
				.replace("https://", "");
		String products[] = data.split("\\[");

		product1 = new HashMap<String, String>();
		product2 = new HashMap<String, String>();
		product3 = new HashMap<String, String>();

		for (int i = 0; i < products.length; i++) {
			String product[] = products[i].replaceAll(",", " ").split("  ");
			switch (i) {
			case 0:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product1.put(key_value[0], key_value[1]);
				}
				break;
			case 1:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product2.put(key_value[0], key_value[1]);
				}
				break;
			case 2:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product3.put(key_value[0], key_value[1]);
				}
				break;
			}
		}
	}

	public static Response sendJsonRequest(String product_id, String name, int age, int amount, int value) {

		RestAssured.baseURI = "https://podium-slns-interview.getsandbox.com/products";

		String payload = "{ \"name\": \"" + name + "\", \"age\": " + age + ", \"mortgageAmount\": " + amount
				+ ", \"propertyValue\": " + value + " }";

		return

		given().urlEncodingEnabled(true).body(payload).header("Content-Type", "application/json")
				.post("/" + product_id + "/applications").then().extract().response();
	}


	@Given("An API call is made for product {string}")
	public static void chooseProduct(String product) {
		product_id = Integer.parseInt(product);
	}

	@And("All product rules are met")
	public static void assertRulesAreMet() {
		switch (product_id) {
		case 1:
			age = 25;
			amount = 4000;
			value = 5000;
			actual_ltv = (float) amount / value;
			expected_ltv = Float.parseFloat(product1.get("loanToValue"));
			Assert.assertTrue(
					age >= Integer.parseInt(product1.get("minAge")) && age <= Integer.parseInt(product1.get("maxAge")));
			Assert.assertTrue(actual_ltv >= expected_ltv);
			break;
		case 2:
			age = 25;
			amount = 6000;
			value = 10000;
			actual_ltv = (float) amount / value;
			expected_ltv = Float.parseFloat(product2.get("loanToValue"));
			Assert.assertTrue(
					age >= Integer.parseInt(product2.get("minAge")) && age <= Integer.parseInt(product2.get("maxAge")));
			Assert.assertTrue(actual_ltv >= expected_ltv);
			break;
		case 3:
			age = 25;
			amount = 2500;
			value = 5000;
			actual_ltv = (float) amount / value;
			expected_ltv = Float.parseFloat(product3.get("loanToValue"));
			Assert.assertTrue(
					age >= Integer.parseInt(product3.get("minAge")) && age <= Integer.parseInt(product3.get("maxAge")));
			Assert.assertTrue(actual_ltv >= expected_ltv);
			break;
		}
	}
	
	@And("The customers age is equal to the minimum age rule")
	public static void ageEqualsMinAge() {
		amount = 2500;
		value = 5000;
		switch (product_id) {
		case 1:
			age = Integer.parseInt(product1.get("minAge"));
		break;
		case 2:
			age = Integer.parseInt(product2.get("minAge"));
		break;
		case 3:
			age = Integer.parseInt(product3.get("minAge"));			
		break;
		}
	}
	
	@And("The customers age is equal to the maximum age rule")
	public static void ageEqualsMaxAge() {
		amount = 2500;
		value = 5000;
		switch (product_id) {
		case 1:
			age = Integer.parseInt(product1.get("maxAge"));
		break;
		case 2:
			age = Integer.parseInt(product2.get("maxAge"));
		break;
		case 3:
			age = Integer.parseInt(product3.get("maxAge"));			
		break;
		}
	}

	@And("The minAge rule is not met")
	public static void minAgeNotMet() {
		amount = 2500;
		value = 5000;
		switch (product_id) {
		case 1:
			age = Integer.parseInt(product1.get("minAge")) - 1;
		break;
		case 2:
			age = Integer.parseInt(product2.get("minAge")) - 1;
		break;
		case 3:
			age = Integer.parseInt(product3.get("minAge")) - 1;			
		break;
		}
	}
	
	@And("The maxAge rule is not met")
	public static void maxAgeNotMet() {
		amount = 2500;
		value = 5000;
		switch (product_id) {
		case 1:
			age = Integer.parseInt(product1.get("maxAge")) + 1;
		break;
		case 2:
			age = Integer.parseInt(product2.get("maxAge")) + 1;
		break;
		case 3:
			age = Integer.parseInt(product3.get("maxAge")) + 1;			
		break;
		}
	}

	@Then("{string} decision is recieved")
	public void verifyDecisionReceived(String expected_decision) {
		Response response = sendJsonRequest(String.valueOf(product_id), "Bob", age, amount, value);
		String decision = response.jsonPath().getString("decision");
		Assert.assertEquals(expected_decision, decision);
	}
	
	@Then("Product not found by that ID error is recieved")
	public void verifyErrorReceived() {		
		Response response = sendJsonRequest(String.valueOf(product_id), "Bob", 25, 2500, 5000);
		String decision = response.jsonPath().getString("reason");
		Assert.assertEquals("[[field:productId, message:Product not found by that ID]]", decision);
	}
	

}
