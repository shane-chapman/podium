Feature: GUI - Mortgage product options and details
Description: Scenarios to assert that the mortgage options display with the correct details
	
	Scenario: Assert that the product details are correct for Lender A
		Given the homepage is displayed
		Then product 1 is displayed with the correct details
		
	Scenario: Assert that the product details are correct for Lender B
		Given the homepage is displayed
		Then product 2 is displayed with the correct details
		
	Scenario: Assert that the product details are correct for Lender C
		Given the homepage is displayed
		Then product 3 is displayed with the correct details