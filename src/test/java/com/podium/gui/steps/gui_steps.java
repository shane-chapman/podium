package com.podium.gui.steps;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.podium.configuration.config;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import junit.framework.Assert;

public class gui_steps extends config {
	
	static HashMap<String, String> product1;
	static HashMap<String, String> product2;
	static HashMap<String, String> product3;
	static int product_id;
	static int age, amount, value;
	static Float actual_ltv;
	static Float expected_ltv;

	protected static void addScreenshot(Scenario scenario) {
		try {
			byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		} catch (RuntimeException e) {
			e.printStackTrace();
		}  
	}
	
	@Before
	public void initDriver() {
		configureDriver();
		PageFactory.initElements(driver, this);
		getProducts();
	}

	@After
	public void closeDriver(Scenario scenario) {
		addScreenshot(scenario);
		driver.close();
		driver.quit();
	}
	
	
	public static Response doGetRequest(String endpoint) {
		RestAssured.defaultParser = Parser.JSON;

		return given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).when().get(endpoint).then()
				.contentType(ContentType.JSON).extract().response();
	}
	
	public void getProducts() {
		RestAssured.baseURI = "https://podium-slns-interview.getsandbox.com";
		RestAssured.defaultParser = Parser.JSON;
		Response response = doGetRequest("/products");

		String data = response.jsonPath().getString("data").replace(":[", "").replace("]", "").replace("[[", "")
				.replace("https://", "");
		String products[] = data.split("\\[");

		product1 = new HashMap<String, String>();
		product2 = new HashMap<String, String>();
		product3 = new HashMap<String, String>();

		for (int i = 0; i < products.length; i++) {
			String product[] = products[i].replaceAll(",", " ").split("  ");
			switch (i) {
			case 0:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product1.put(key_value[0], key_value[1]);
				}
				break;
			case 1:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product2.put(key_value[0], key_value[1]);
				}
				break;
			case 2:
				for (String prod : product) {
					String key_value[] = prod.split(":");
					product3.put(key_value[0], key_value[1]);
				}
				break;
			}
		}
	}

	@Given("the homepage is displayed")
	public void assertHomepageDisplayed() {
	driver.get("https://podium-test-65804.firebaseapp.com/");

	}

	@Then("product {int} is displayed with the correct details")
	public void assertProductDetails(int product) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("(//div[@class='header'])["+product+"]")));
		WebElement product_name = driver.findElement(By.xpath("(//div[@class='header'])["+product+"]"));
		WebElement product_price = driver.findElement(By.xpath("(//span[@class='price'])["+product+"]"));
		WebElement product_rate = driver.findElement(By.xpath("(//span[@class='stay'])["+product+"]"));
		WebElement product_desc = driver.findElement(By.xpath("(//div[@class='description'])["+product+"]"));
		WebElement product_image = driver.findElement(By.xpath("(//div[@class='ui small image'])["+product+"]//img"));
		switch(product) {
		case 1:
		Assert.assertEquals(product1.get("name"), product_name.getText());
		Assert.assertEquals(product1.get("interestRate")+"%", product_rate.getText());
		Assert.assertEquals("£"+product1.get("monthlyRepayment"), product_price.getText());
		Assert.assertEquals("Mortgage provided by "+product1.get("lendername"), product_desc.getText());
		Assert.assertEquals("https://"+product1.get("logo"), product_image.getAttribute("src"));
		break;
		case 2:
		Assert.assertEquals(product2.get("name"), product_name.getText());
		Assert.assertEquals(product2.get("interestRate")+"%", product_rate.getText());
		Assert.assertEquals("£"+product2.get("monthlyRepayment"), product_price.getText());
		Assert.assertEquals("Mortgage provided by "+product2.get("lendername"), product_desc.getText());
		Assert.assertEquals("https://"+product2.get("logo"), product_image.getAttribute("src"));
		break;
		case 3:
		Assert.assertEquals(product3.get("name"), product_name.getText());
		Assert.assertEquals(product3.get("interestRate")+"%", product_rate.getText());
		Assert.assertEquals("£"+product3.get("monthlyRepayment"), product_price.getText());
		Assert.assertEquals("Mortgage provided by "+product3.get("lendername"), product_desc.getText());
		Assert.assertEquals("https://"+product3.get("logo"), product_image.getAttribute("src"));
		break;
		}

	}

}
